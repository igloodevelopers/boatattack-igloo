﻿Shader "Unlit/CompositeShader"
{
    Properties
    {
	    [NoScaleOffset] _MainTex ("Texture", 2D) = "white" {}
		[NoScaleOffset] _CompTex ("Overlay", 2D) = "white" {}
		_CompOffset("Texture2_Offset", Vector) = (-3.5, -0.5, 0, 0)
		_CompTile("Texture2_Tile", Vector) = (8, 2, 0, 0)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
			Blend One Zero, One Zero
			Cull Back
			ZTest LEqual
			ZWrite On

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

			float2 _CompOffset;
			float2 _CompTile;
			sampler2D(_MainTex); float4 _MainTex_TexelSize;
			sampler2D(_CompTex); float4 _CompTex_TexelSize;

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
                return col;
            }
            ENDCG
        }
    }
}
